# frozen_string_literal: true

Resource.create(name: 'Water', points: 4)
Resource.create(name: 'Food', points: 3)
Resource.create(name: 'Medication', points: 2)
Resource.create(name: 'Ammunition', points: 1)
Survivor.create(name: 'Test', age: 25, gender: 'Male', infected: false, latitude: -25.39018995, longitude: -51.46200833)
